#include "mpich-x86_64/mpi.h"
#include <iostream>
#include <cstring>

int main(int argc, char** argv) {
    int rank;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    int processCount;
    MPI_Comm_size(MPI_COMM_WORLD, &processCount);

    char* sendBuff = new char[]{"Text"};
    int sendBuffSize = strlen(sendBuff) + 1;
    int recvBufferSize = sendBuffSize * processCount;
    char* recvBuff = new char[recvBufferSize];

    MPI_Allgather(sendBuff, sendBuffSize, MPI_CHAR, recvBuff, sendBuffSize, MPI_CHAR, MPI_COMM_WORLD);
    if (rank == 2) {
        std::cout << "\nAllgather:" << std::endl;
        for (int i = 0; i < recvBufferSize; i++) {
            std::cout << recvBuff[i];
        }
        std::cout << std::endl;
    }

    MPI_Allgatherv(
            sendBuff,
            sendBuffSize,
            MPI_CHAR,
            recvBuff,
            new int[]{5, 5, 5},
            new int[]{0, 4, 10},
            MPI_CHAR,
            MPI_COMM_WORLD
    );

    if (rank == 2) {
        std::cout << "\nAllgatherv:" << std::endl;
        for (int i = 0; i < recvBufferSize; i++) {
            std::cout << recvBuff[i];
        }
        std::cout << std::endl;
    }

    MPI_Finalize();
}