#include <iostream>
#include <mpi.h>

int main(int argc, char** argv) {
    int rank, size;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    MPI_Group world_group;
    MPI_Comm_group(MPI_COMM_WORLD, &world_group);

    MPI_Group add_remove_group;
    int ranks_to_add[] = {1, 2}; // Ранги для добавления

    MPI_Group_incl(world_group, 2, ranks_to_add, &add_remove_group);
    int groupSize = 0;
    int* trRanks= new int[2];

    MPI_Group_translate_ranks(world_group, 2, ranks_to_add, add_remove_group, trRanks);

    if (rank == 0) {
        std::cout << trRanks[0] << ',' << trRanks[1] << ", ";
    }
    MPI_Group_excl(add_remove_group, 1, trRanks, &add_remove_group);
    MPI_Group_size(add_remove_group, &groupSize);

    if (rank == 0) {
        std::cout << groupSize << ", ";
    }

    MPI_Comm new_comm;
    MPI_Comm_create_group(MPI_COMM_WORLD, add_remove_group, 0, &new_comm);

    int new_rank, new_size;
    if (new_comm != MPI_COMM_NULL) {
        MPI_Comm_rank(new_comm, &new_rank);
        MPI_Comm_size(new_comm, &new_size);
        std::cout << "\nНовый коммуникатор: локальный ранг " << new_rank << " из " << new_size - 1 << std::endl;
        std::cout << "Глобальный ранг " << rank << " из " << size - 1 << std::endl;
    }

    MPI_Group_free(&world_group);
    MPI_Group_free(&add_remove_group);
    if (new_comm != MPI_COMM_NULL) {
        MPI_Comm_free(&new_comm);
    }

    MPI_Finalize();
}
