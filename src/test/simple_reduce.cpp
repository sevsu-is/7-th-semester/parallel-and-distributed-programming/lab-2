#include "mpich-x86_64/mpi.h"
#include <iostream>

int main(int argc, char** argv) {
    int rank, processCount;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &processCount);

    int* numbers = new int[]{2, 5, 10};
    int* result = new int[3];

    MPI_Reduce(numbers, result, 3, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);

    if (rank == 0) {
        for (int i = 0; i < 3; i++) {
            std::cout << result[i] << ", ";
        }
    }

    MPI_Finalize();
}