#include "mpich-x86_64/mpi.h"
#include <iostream>

int main(int argc, char** argv) {
    int rank, processCount;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &processCount);

    char* sendBuff = new char[]{"Ran0Ran1Ran2"};
    int recvBufferSize = 5;
    char* recvBuff = new char[] {"____"};

    if (rank == 1) {
        std::cout << "\nRecv buffer in process #1 before Scatter:" << std::endl;
        for (int i = 0; i < recvBufferSize; i++) {
            std::cout << recvBuff[i];
        }
        std::cout << std::endl;
    }

    MPI_Scatter(sendBuff, 4, MPI_CHAR,recvBuff,4,MPI_CHAR,0,MPI_COMM_WORLD);

    if (rank == 1) {
        std::cout << "\nRecv buffer in process #1 after Scatter:" << std::endl;
        for (int i = 0; i < recvBufferSize; i++) {
            std::cout << recvBuff[i];
        }
        std::cout << std::endl;
    }

    MPI_Finalize();
}