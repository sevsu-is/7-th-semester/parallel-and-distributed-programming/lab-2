#include "mpich-x86_64/mpi.h"
#include <iostream>

int main(int argc, char** argv) {
    int rank;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    int groupSize, root = 0;
    int* receivingBuffer;
    int sendingBuffer[10] = {0};

    for (int& i : sendingBuffer) {
        i = rank;
    }

    MPI_Comm_size(MPI_COMM_WORLD, &groupSize);
    receivingBuffer = new int[groupSize * 10];
    MPI_Gather(sendingBuffer, 10, MPI_INT, receivingBuffer, 10, MPI_INT, root, MPI_COMM_WORLD);

    if (rank == 0) {
        std::cout << "Root: ";
        for (int i = 0; i < groupSize * 10; i++) {
            std::cout << receivingBuffer[i];
        }
        std::cout << std::endl;
    } else {
        std::cout << "Rank " << rank << " Data: ";
        for (int& i : sendingBuffer) {
            std::cout << i;
        }
        std::cout << std::endl;
    }

    MPI_Finalize();
}