#include <iostream>
#include <mpi.h>

// Функция пользовательской операции для вычисления локального минимума
void min_local(void* in, void* inout, int* len, MPI_Datatype* datatype) {
    int* in_data = (int*) in;
    int* inout_data = (int*) inout;

    for (int i = 0; i < *len; i++) {
        if (in_data[i] < inout_data[i]) {
            inout_data[i] = in_data[i];
        }
    }
}

int main(int argc, char** argv) {
    int rank, size;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    // Локальное значение, разное для каждого процесса
    int local_value = (rank + 1) * 2;
    // Глобальное минимальное значение
    int global_min;

    MPI_Op min_op;
    MPI_Op_create(min_local, 0, &min_op); // Создание пользовательской операции
    MPI_Reduce(&local_value, &global_min, 1, MPI_INT, min_op, 0, MPI_COMM_WORLD);

    if (rank == 0) {
        std::cout << "Глобальное минимальное значение: " << global_min << std::endl;
    }

    MPI_Op_free(&min_op); // Освобождение операции
    MPI_Finalize();
}
