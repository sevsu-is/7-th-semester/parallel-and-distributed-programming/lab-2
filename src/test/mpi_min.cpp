#include <iostream>
#include <mpi.h>

int main(int argc, char** argv) {
    int rank, size;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    // Локальное значение, разное для каждого процесса
    int local_value = (rank + 1) * 2;
    // Глобальное минимальное значение
    int global_min;

    MPI_Reduce(&local_value, &global_min, 1, MPI_INT, MPI_MIN, 0, MPI_COMM_WORLD);

    if (rank == 0) {
        std::cout << "Глобальное минимальное значение: " << global_min << std::endl;
    }

    MPI_Finalize();
}