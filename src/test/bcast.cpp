#include "mpich-x86_64/mpi.h"
#include <iostream>
#include <cstring>

int main(int argc, char** argv) {
    int rank;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    char* buffer;
    if (rank != 0) {
        buffer = new char[]{"____"};
    } else {
        buffer = new char[]{"Root"};
    }

    if (rank == 1) {
        std::cout << "Before Bcast from root" << std::endl;
        for (int i = 0; i < strlen(buffer); i++) {
            std::cout << buffer[i];
        }
        std::cout << std::endl;
    }

    MPI_Bcast(buffer, strlen(buffer) + 1, MPI_CHAR, 0, MPI_COMM_WORLD);

    if (rank == 1) {
        std::cout << "After Bcast from root" << std::endl;
        for (int i = 0; i < strlen(buffer); i++) {
            std::cout << buffer[i];
        }
        std::cout << std::endl;
    }

    MPI_Finalize();
}