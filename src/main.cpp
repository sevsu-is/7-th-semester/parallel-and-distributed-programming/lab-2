#include <iostream>
#include <mpi.h>
#include <cmath>

double rectangleRule(double a, double b, int n) {
    double h = (b - a) / n; // Ширина каждого прямоугольника
    double integral = 0.0;

    for (int i = 0; i < n; i++) {
        double x = a + i * h + h / 2.0; // Середина прямоугольника
        integral += exp(x) * h; // Площадь прямоугольника: f(x) * h
    }
    return integral;
}

int main(int argc, char** argv) {
    int rank, commSize;
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &commSize);

    auto* broadcastMessageBuffer = new double[3];
    auto* localResult = new double { 0 };
    auto* rootResult = new double;
    int n = 1000;

    if (rank == 0) {
        double a = 0.0;
        double b = 1.0;
        std::cout << "Интервал интегрирования (от до): ";
        std::cin >> a >> b;

        broadcastMessageBuffer[0] = a;
        broadcastMessageBuffer[1] = b;
        broadcastMessageBuffer[2] = commSize;

        MPI_Bcast(broadcastMessageBuffer, 3, MPI_DOUBLE, 0, MPI_COMM_WORLD);
        MPI_Reduce(localResult, rootResult, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

        std::cout << "Значение интеграла exp(x) на интервале [" << a << "; " << b << "] при n = " << n << " равно: "
                  << *rootResult << std::endl;
    } else {
        MPI_Bcast(broadcastMessageBuffer, 3, MPI_DOUBLE, 0, MPI_COMM_WORLD);

        double a = broadcastMessageBuffer[0];
        double b = broadcastMessageBuffer[1];
        double size = broadcastMessageBuffer[2];

        double len = (b - a) / (size - 1);
        double localA = a + (rank - 1) * len;
        double localB = localA + len;

        *localResult = rectangleRule(localA, localB, n);
        MPI_Reduce(localResult, rootResult, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
    }

    MPI_Finalize();
}
